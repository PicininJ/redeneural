# -*- coding: utf-8 -*-
"""
Created on Thu May  9 10:51:26 2019

@author: Aluno
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

base = []
for i in range(0,4):
    base.append(pd.read_csv(str(i)+".csv",header=None))
    
baseMerge = pd.concat(base)

from sklearn.utils import shuffle
baseAleatoria = shuffle(baseMerge)

baseAleatoria.to_csv('Dataset.csv')

from sklearn.model_selection import train_test_split
train, test = train_test_split(baseAleatoria, test_size=0.4)

train.to_csv('Dataset_Treino_60.csv')
test.to_csv('Dataset_teste_40.csv')

train, test = train_test_split(baseAleatoria, test_size=0.8)

train.to_csv('Dataset_Treino_20.csv')
test.to_csv('Dataset_teste_80.csv')

