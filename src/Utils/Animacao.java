/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.util.ArrayList;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.XYChart;

/**
 *
 * @author luis
 */
public class Animacao implements Runnable {

    private AreaChart<?, ?> lcGrafico;
    private ArrayList<ArrayList<Double>> erros;

    public Animacao(AreaChart<?, ?> lcGrafico, ArrayList<ArrayList<Double>> erros) {
        this.lcGrafico = lcGrafico;
        this.erros = erros;
    }

    public Animacao(ArrayList<Double> erro, AreaChart<?, ?> lcGrafico) {
        this.lcGrafico = lcGrafico;
        erros = new ArrayList();
        erros.add(erro);
    }

    @Override
    public void run() {
        lcGrafico.getData().clear();
        XYChart.Series seriesLin;
        for (int i = 0; i < erros.size(); i++) {
            seriesLin = new XYChart.Series();
            for (Integer j = 0; j < erros.get(i).size(); j++) {
                seriesLin.getData().add(new XYChart.Data<>(j.toString(), erros.get(i).get(j)));
            }
            if (erros.size() < 2) {
                seriesLin.setName("Erro");
            } else {
                seriesLin.setName("Erro: " + i);
            }
            lcGrafico.getData().add(seriesLin);
        }
    }

}
