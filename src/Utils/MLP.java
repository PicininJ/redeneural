/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import RedeNeural.Avaliacao.Accuracy;
import RedeNeural.Avaliacao.Avaliacao;
import RedeNeural.Avaliacao.ConfusionMatrix;
import RedeNeural.Avaliacao.ErroRede;
import RedeNeural.Core.Dataset;
import RedeNeural.Interface.FXMLDocumentController;
import RedeNeural.Neuronio.Neuronio;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Raizen
 */

public class MLP {
/*
    private int Entrada;
    private int Saida;
    private int Oculta;
    private double N;
    private int nInteracoes;
    private double Erro;
    private String Parada;
    private String FuncaodeTransferencia;
    private Dataset Treino;
    private Dataset Teste;
    int TipoDeParada;

    public double ErroRede;

    /////Camadas
    public double[] CamadaEntrada;
    public Neuronio[] CamadaSaida;
    public Neuronio[] CamadaOculta;

    ////Pesos
    public double[][] PesosOculta;
    public double[][] PesosSaida;
    public ErroRede erroEpoca;
    private Avaliacao avaliacao;

    public MLP(int Entrada, int Saida, int Oculta, double N, int nInteracoes,
            String Parada, String FuncaodeTransferencia, double Erro) {
        this.Entrada = Entrada;
        this.Saida = Saida;
        this.Oculta = Oculta;
        this.N = N;
        this.nInteracoes = nInteracoes;
        this.Parada = Parada;
        this.FuncaodeTransferencia = FuncaodeTransferencia;
        this.Erro = Erro;

        Neuronio.setFuncao(this.FuncaodeTransferencia);

        if (this.FuncaodeTransferencia.equals("Por Iteração")) {
            TipoDeParada = 1;
        } else if (this.FuncaodeTransferencia.equals("Por Erro")) {
            TipoDeParada = 2;
        }

        InicializaCamadaSaida();
        InicializaCamadaEntrada();
        InicializaCamadaOculta();
    }

    private void InicializaCamadaOculta() {
        CamadaOculta = new Neuronio[Oculta];
        for (int i = 0; i < Oculta; i++) {
            CamadaOculta[i] = new Neuronio();
        }

        PesosOculta = new double[Oculta][Entrada];
        PesosSaida = new double[Saida][Oculta];
        for (int i = 0; i < CamadaSaida.length; i++) {
            CamadaSaida[i] = new Neuronio();
            GeraPeso(Entrada, Saida);
        }
    }

    private void InicializaCamadaSaida() {
        CamadaSaida = new Neuronio[Saida];
    }

    private void InicializaCamadaEntrada() {
        CamadaEntrada = new double[Entrada];
    }

    public void fit(Dataset Treino) {
        
        this.Treino = Treino;
        erroEpoca = new ErroRede();

        double ErroIteracao = 0;
        boolean First = true;
        int Count = 0;
        while (Count < nInteracoes && ErroRede >= Erro || Count < nInteracoes && First) {
            for (int i = 0; i < Treino.get().size() && ErroRede > Erro || First; i++) {
                First = false;
                ArrayList<Double> valor = Treino.get().get(i);

                int l = 0;
                while (l < valor.size() - 1) {
                    CamadaEntrada[l] = valor.get(l++);
                }
                int Desejado = valor.get(l).intValue();

                double ErroLinha = 0;
                //calcula o net de cada neuronio da oculta
                for (int j = 0; j < Oculta; j++) {
                    CamadaOculta[j].calculaNet(CamadaEntrada, PesosOculta, j);
                    CamadaOculta[j].getFuncao().Calcula();
                }

                /////Pega o Erro
                double[] vOculta = new double[Oculta];
                for (int k = 0; k < Oculta; k++) {
                    vOculta[k] = CamadaOculta[k].getObtido();
                }

                //calcula o net de cada neuronio da Saida
                for (int j = 0; j < Saida; j++) {
                    CamadaSaida[j].calculaNet(vOculta, PesosSaida, j);
                    CamadaSaida[j].getFuncao().Calcula();
                    CamadaSaida[j].calculaErroSaida(Desejado);
                }

                //volta com o erro das camadas ocultas
                double[] ErrosDaSaidas = new double[Saida];
                for (int k = 0; k < Saida; k++) {
                    ErrosDaSaidas[k] = CamadaSaida[k].getErro();
                }

                // calcula erro das ocultas
                for (int j = 0; j < Oculta; j++) {
                    CamadaOculta[j].calculaErroOcultas(ErrosDaSaidas, PesosSaida, j);
                }

                //volta
                corrigePesosSaida(N, ErrosDaSaidas);
                corrigePesosOculta(N, CamadaEntrada);

                //calcula erro da rede e soma no erro da iteracao
                CalculoErroRede();
                ErroLinha = ErroRede;
                ErroIteracao += ErroLinha;
                
                //StringBuilder str = new StringBuilder().append("Itr: ").append(Count).append(" Erro Itr: ").append(ErroLinha).append("\n");
                //System.out.println(str);
                //FXMLDocumentController.txbRes.appendText(str.toString());
            }
            
            erroEpoca.add(ErroIteracao);
            Count++;
        }
        System.out.println("Itr: "+nInteracoes);
    }

    public void predict(Dataset Teste) {
        this.Teste = Teste;
        int countA = 0;
        int countE = 0;
        this.avaliacao = new Avaliacao();
        avaliacao.init(Teste.get().size());
        double ErroIteracao = 0;
        int[][] matConf = null;
        for (int i = 0; i < Teste.get().size(); i++) {
            ArrayList<Double> valor = Teste.get().get(i);

            int l = 0;
            while (l < valor.size() - 1) {
                CamadaEntrada[l] = valor.get(l++);
            }
            int Desejado = valor.get(l).intValue();

            double ErroLinha = 0;
            //calcula o net de cada neuronio da oculta
            for (int j = 0; j < Oculta; j++) {
                CamadaOculta[j].calculaNet(CamadaEntrada, PesosOculta, j);
                CamadaOculta[j].getFuncao().Calcula();
            }
            /////Pega o Erro
            double[] vOculta = new double[Oculta];
            for (int k = 0; k < Oculta; k++) {
                vOculta[k] = CamadaOculta[k].getObtido();
            }

            double[] vetObtido = new double[Saida];
            //calcula o net de cada neuronio da Saida
            for (int j = 0; j < Saida; j++) {
                CamadaSaida[j].calculaNet(vOculta, PesosSaida, j);
                CamadaSaida[j].getFuncao().Calcula();
                vetObtido[j] = CamadaSaida[j].getObtido();
            }
            

            avaliacao.predict(vetObtido, Saida, Desejado);
            
            double maior = vetObtido[0];
            double medio = vetObtido[0];
            int posMaior = 0;
            for (int k = 1; k < Saida; k++) {
                if (maior < vetObtido[k]) {
                    maior = vetObtido[k];
                    posMaior = k;
                }
                medio += vetObtido[k];
            }
            medio = medio / Saida;
            BigDecimal valorM = new BigDecimal(maior);
            valorM.setScale(5, RoundingMode.HALF_DOWN);
            maior = valorM.doubleValue();
            BigDecimal valorD = new BigDecimal(Desejado);
            valorD.setScale(5, RoundingMode.HALF_DOWN);

            if (Math.round(maior) == Math.round(valorD.doubleValue())) {
                countA++;
            } else {
                countE++;
            }
        }
        
        ConfusionMatrix cm = new ConfusionMatrix(-1);
        cm.fitShow(avaliacao.getClasseTeste(), avaliacao.getClassePredita());
        Accuracy acc = new Accuracy(-1);
        acc.fitShow(avaliacao.getClasseTeste(), avaliacao.getClassePredita());
        
        StringBuilder result = new StringBuilder();
        if (countE != 0) {
            result.append(" Acertos: ").append(countA).append(" Erros: ").append(countE)
                    .append(" Porcentagem: ").append((countA * 100) / (countE + countA)).append("%");
        } else if (countA != 0) {
            result.append(" Acertos: ").append(countA).append(" Erros: ").
                    append(countE).append(" Porcentagem: 100%");
        } else {
            result.append("Erro nenhum acerto e nenhum Erro!!!!!!");
        }
        result.append("\n").append(result).append("\n");
        System.out.println(result);
        //FXMLDocumentController.txbRes.appendText(result.toString());
    }

    public void corrigePesosOculta(double txAp, double[] aCamadaEntrada) {
        for (int i = 0; i < CamadaOculta.length; i++) {
            for (int j = 0; j < aCamadaEntrada.length - 1; j++) {
                PesosOculta[i][j] = PesosOculta[i][j] + (txAp * CamadaOculta[i].getErro() * aCamadaEntrada[j]);
            }
        }
    }

    public void corrigePesosSaida(double txAp, double[] erro) {
        for (int i = 0; i < erro.length; i++) {
            for (int j = 0; j < CamadaOculta.length; j++) {
                PesosSaida[i][j] = PesosSaida[i][j] + (txAp * erro[i] * CamadaOculta[j].getObtido());
            }
        }
    }

    public void GeraPeso(int Entrada, int Saida) {
        Random num = new Random();
        for (int i = 0; i < CamadaOculta.length; i++) {
            for (int j = 0; j < Entrada; j++) {
                PesosOculta[i][j] = (-2) + num.nextDouble() * (2 - (-2));
            }
        }

        for (int i = 0; i < Saida; i++) {
            for (int j = 0; j < CamadaOculta.length; j++) {
                PesosSaida[i][j] = (-2) + num.nextDouble() * (2 - (-2));
            }
        }

    }

    public void CalculoErroRede() {
        for (int i = 0; i < CamadaSaida.length; i++) {
            ErroRede = ErroRede + Math.pow(CamadaSaida[i].getErro(), 2);
        }
        //ErroRede = ErroRede * 0.5;
    }
*/
}
