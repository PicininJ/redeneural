/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedeNeural.Avaliacao;

import java.util.ArrayList;

/**
 *
 * @author Remote
 */
public class Avaliacao {

    private int[] ClasseTeste;
    private int[] ClassePredita;
    
    private int Amostras;

    private ArrayList<nodeAvaliacao> nodes;

    public void init(int Amostras) {
        ClasseTeste = new int[Amostras];
        ClassePredita = new int[Amostras];
        nodes = new ArrayList();
        this.Amostras = Amostras;
    }

    public void predict(double[] Obtido, int Saidas, int Desejado) {
        nodeAvaliacao nd = new nodeAvaliacao();
        nd.predict(Obtido, Saidas, Desejado);
        nodes.add(nd);
    }

    public int[] getClasseTeste() {
        if (Amostras != nodes.size()) {
            ClasseTeste = new int[nodes.size()];
        }
        for (int i = 0; i < nodes.size(); i++) {
            ClasseTeste[i] = nodes.get(i).getDesejado();
        }
        return ClasseTeste;
    }

    public int[] getClassePredita() {
        if (Amostras != nodes.size()) {
            ClassePredita = new int[nodes.size()];
        }
        for (int i = 0; i < nodes.size(); i++) {
            ClassePredita[i] = nodes.get(i).getMax();
        }
        return ClassePredita;
    }
}
