/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedeNeural.Avaliacao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author Aluno
 */
public class ConfusionMatrix extends Metrics {

    private double[][] Matrix;
    private int Classes;

    public ConfusionMatrix(int Classes) {
        this.Classes = Classes;
        if (Classes > 0) {
            generate();
        }
    }

    public void generate() {
        Matrix = new double[Classes][];
        for (int i = 0; i < Classes; i++) {
            Matrix[i] = new double[Classes];
            for (int j = 0; j < Classes; j++) {
                Matrix[i][j] = 0;
            }
        }
    }

    public ConfusionMatrix() {
        Classes = -1;
    }

    public double[][] fit(int[] ClassE, int[] ClassT) {

        if (Classes == -1) {
            Classes = super.count(ClassE, ClassT);
            generate();
        }

        if (ClassE.length == ClassT.length) {
            for (int i = 0; i < ClassE.length; i++) {
                if (ClassT[i] >= Classes) {
                    ClassT[i] = Classes - 1;
                }
                if (ClassE[i] >= Classes) {
                    ClassE[i] = Classes - 1;
                }
                if (ClassT[i] > -1 && ClassE[i] > -1) {
                    Matrix[ClassE[i]][ClassT[i]] += 1;
                } else if (ClassE[i] > -1) {
                    Matrix[ClassE[i]][ClassE[i]] += 1;
                } else {
                    Matrix[ClassE[i]][ClassE[i]] += 1;
                }
            }
        }
        return getMatrix();
    }

    public double[][] fit2(int[] ClassE, int[] ClassT) {

        if (Classes == -1) {
            Classes = super.count(ClassE, ClassT);
            generate();
        }

        if (ClassE.length == ClassT.length) {
            for (int i = 0; i < ClassE.length; i++) {
                if (ClassT[i] >= Classes) {
                    ClassT[i] = Classes - 1;
                }
                if (ClassE[i] >= Classes) {
                    ClassE[i] = Classes - 1;
                }
                if (ClassT[i] > 0 && ClassE[i] > 0) {
                    Matrix[ClassE[i] - 1][ClassT[i] - 1] += 1;
                } else if (ClassE[i] > 0) {
                    Matrix[ClassE[i] - 1][ClassE[i] - 1] += 1;
                } else {
                    Matrix[ClassE[i]][ClassE[i]] += 1;
                }
            }
        }
        return getMatrix();
    }

    public double[][] fitOld(int[] ClassE, int[] ClassT) {

        if (Classes == -1) {
            Classes = super.count(ClassE, ClassT);
            generate();
        }

        if (ClassE.length == ClassT.length) {
            for (int i = 0; i < ClassE.length; i++) {
                if (ClassE[i] > 0 && ClassE[i] < ClassE.length) {
                    if (ClassT[i] > 0 && ClassT[i] < ClassT.length) {
                        Matrix[ClassE[i] - 1][ClassT[i] - 1] += 1;
                    } else if (ClassT[i] > 0) {
                        Matrix[ClassE[i] - 1][ClassT[i]] += 1;
                    } else {
                        Matrix[ClassE[i] - 1][0] += 1;
                    }
                } else if (ClassE[i] > 0) {
                    if (ClassT[i] > 0 && ClassT[i] < ClassT.length) {
                        Matrix[ClassE[i]][ClassT[i] - 1] += 1;
                    } else if (ClassT[i] > 0) {
                        Matrix[ClassE[i]][ClassT[i]] += 1;
                    } else {
                        Matrix[ClassE[i]][0] += 1;
                    }
                } else {
                    if (ClassT[i] > 0 && ClassT[i] < ClassT.length) {
                        Matrix[0][ClassT[i] - 1] += 1;
                    } else if (ClassT[i] > 0) {
                        Matrix[0][ClassT[i]] += 1;
                    } else {
                        Matrix[0][0] += 1;
                    }
                }
            }
        }
        return getMatrix();
    }

    public void fitShow(int[] ClassE, int[] ClassT) {
        fit(ClassE, ClassT);
        StringBuilder st = new StringBuilder();
        st.append("Confusion Matrix\n");
        for (int i = 0; i < Classes; i++) {
            st.append("[ ");
            for (int j = 0; j < Classes; j++) {
                st.append(String.format("%3d  ", (int) getMatrix()[i][j]));
            }
            st.append("] \n");
        }
        System.out.println(st);
    }

    public void fitShow(ArrayList<Avaliacao> avaliacoes) {
        if (avaliacoes != null && avaliacoes.size() > 0) {
            int[] ClassE = avaliacoes.get(0).getClasseTeste();
            int[][] ClassTM = new int[avaliacoes.get(0).getClassePredita().length][];
            for (int i = 0; i < ClassTM.length; i++) {
                ClassTM[i] = new int[avaliacoes.size()];
            }
            for (int i = 0; i < avaliacoes.size(); i++) {
                for (int j = 0; j < avaliacoes.get(i).getClassePredita().length; j++) {
                    ClassTM[j][i] = avaliacoes.get(i).getClassePredita()[j];
                }
            }
            int[] ClassT = new int[ClassTM.length];
            int Count = 0;
            for (int i = 0; i < ClassTM.length; i++) {
                ClassT[Count++] = ClassTM[i][(0 + ClassTM[i].length - 1) / 2];
            }
            fitShow(ClassE, ClassT);
        }
    }

    public static void main(String[] args) {
        TestRandom(10);
        TestClass(1, 10);
        TestDiagonalPrincipal(10);
    }

    /**
     *
     * @param classes
     * @test
     */
    public static void TestRandom(int classes) {
        if (classes == 0) {
            classes = 3;
        }
        ConfusionMatrix cm = new ConfusionMatrix(classes);
        Random r = new Random();
        int TF = r.nextInt(50);
        int[] ClassE = new int[TF];
        for (int i = 0; i < TF; i++) {
            ClassE[i] = r.nextInt(classes);
        }
        int[] ClassT = new int[TF];
        for (int i = 0; i < TF; i++) {
            ClassT[i] = r.nextInt(classes);
        }
        cm.fitShow(ClassE, ClassT);
    }

    /**
     *
     * @param classes
     * @test
     */
    public static void TestClass(int classe, int classes) {
        if (classes == 0) {
            classes = 3;
        }
        if (classes > classe) {
            ConfusionMatrix cm = new ConfusionMatrix(classes);
            Random r = new Random();
            int TF = r.nextInt(50);
            int[] ClassE = new int[TF];
            for (int i = 0; i < TF; i++) {
                ClassE[i] = classe;
            }
            int[] ClassT = new int[TF];
            for (int i = 0; i < TF; i++) {
                ClassT[i] = classe;
            }
            cm.fitShow(ClassE, ClassT);
        }
    }

    /**
     *
     * @param classes
     * @test
     */
    public static void TestDiagonalPrincipal(int classes) {
        if (classes == 0) {
            classes = 3;
        }
        ConfusionMatrix cm = new ConfusionMatrix(classes);
        Random r = new Random();
        int TF = r.nextInt(50);
        int classe;
        int[] ClassE = new int[TF];
        int[] ClassT = new int[TF];
        for (int i = 0; i < TF; i++) {
            classe = r.nextInt(classes);;
            ClassE[i] = classe;
            ClassT[i] = classe;
        }
        cm.fitShow(ClassE, ClassT);
    }

    /**
     * @return the Matrix
     */
    public double[][] getMatrix() {
        return Matrix;
    }
}
