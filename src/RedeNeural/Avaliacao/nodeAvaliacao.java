/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedeNeural.Avaliacao;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author Remote
 */
public class nodeAvaliacao {

    private Double max;
    private Double min;
    private Double mid;
    private double Desejado;

    ///Calcula os Acertos e os Erros
    public void predict(double[] Obtido, int Saidas, int Desejado) {
        max = Obtido[0];
        mid = Obtido[0];
        min = Obtido[0];
        this.Desejado = Desejado;

        int posMaior = 0;
        int posMenor = 0;
        int posMid = 0;

        for (int k = 1; k < Saidas; k++) {
            if (max < Obtido[k]) {
                max = Obtido[k];
                posMaior = k;
            }
            if (min > Obtido[k]) {
                min = Obtido[k];
                posMenor = k;
            }
            mid += Obtido[k];
        }

        mid = mid / Saidas;
        posMid = 0;
        double valMid = Obtido[0] - mid;
        double aux;
        for (int k = 1; k < Saidas; k++) {
            aux = Obtido[k]-mid;
            if(aux < valMid){
                posMid = k;
                valMid = aux;
            }
        }
        
        if(posMaior == Desejado){
            max = (double)Desejado;
        }else{
            max = (double)posMaior;
        }
        
        if(posMenor == Desejado){
            min = (double)Desejado;
        }else{
            min = (double)posMenor;
        }
        
        if(posMid == Desejado){
            mid = (double)Desejado;
        }else{
            mid = (double)posMid;
        }
        
    }

    ///Calcula os Acertos e os Erros
    public void predictOld(double[] Obtido, int Saidas, int Desejado) {
        max = Obtido[0];
        mid = Obtido[0];
        min = Obtido[0];
        this.Desejado = Desejado;

        int posMaior = 0;
        int posMenor = 0;

        for (int k = 1; k < Saidas; k++) {
            if (max < Obtido[k]) {
                max = Obtido[k];
                posMaior = k;
            }
            if (min > Obtido[k]) {
                min = Obtido[k];
                posMenor = k;
            }
            mid += Obtido[k];
        }

        mid = mid / Saidas;
    }

    /**
     * @return the max
     */
    public int getMax() {
        return transform(max);
    }

    /**
     * @return the min
     */
    public int getMin() {
        return transform(min);
    }

    /**
     * @return the mid
     */
    public int getMid() {
        return transform(mid);
    }

    /**
     * @return the Desejado
     */
    public int getDesejado() {
        return transform(Desejado);
    }

    public int transform(Double value) {
        int val;
        try {
            BigDecimal valorD = new BigDecimal(value);
            valorD.setScale(5, RoundingMode.HALF_DOWN);
            val = (int) Math.round(valorD.doubleValue());
        } catch (Exception ex) {
            val = getDesejado();
            System.out.println(value);
            ////System.out.println("Exc: nodeAvaliacao:83");
        }
        return val;
    }

}
