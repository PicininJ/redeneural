/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedeNeural.Avaliacao;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Remote
 */
public class ErroRede {
    private ArrayList<Double> Erro;
    private void init(){
        if(Erro == null){
            Erro = new ArrayList();
        }
    }
    public void add(Double ErroEpoca){
        init();
        Erro.add(ErroEpoca);
    }
    public ArrayList<Double> get(){
        return Erro;
    }

    public void fit(ArrayList<ErroRede> erro) {
        if (erro != null && erro.size() > 0) {
            double[][] ClassTM = new double[erro.get(0).get().size()][];
            for (int i = 0; i < ClassTM.length; i++) {
                ClassTM[i] = new double[erro.size()];
            }
            for (int i = 0; i < erro.size(); i++) {
                for (int j = 0; j < erro.get(i).get().size(); j++) {
                    ClassTM[j][i] = erro.get(i).get().get(j);
                }
            }
            for (int i = 0; i < ClassTM.length; i++) {
                add(Arrays.stream(ClassTM[i]).sum()/ClassTM[i].length);
            }
        }
    }
}
