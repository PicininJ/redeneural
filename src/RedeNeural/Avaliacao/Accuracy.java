/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedeNeural.Avaliacao;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Remote
 */
public class Accuracy extends Metrics {

    private double acc;
    private int acertos;
    private int Classes;

    public Accuracy(int Classes) {
        this.Classes = Classes;
        this.acc = 0;
        this.acertos = 0;
    }

    public Accuracy() {
        this.Classes = -1;
        this.acc = 0;
        this.acertos = 0;
    }

    public double fit(int[] ClassE, int[] ClassT) {

        if (Classes == -1) {
            Classes = super.count(ClassE, ClassT);
        }

        int CountTotal = 0;
        if (ClassE.length == ClassT.length) {
            CountTotal = ClassE.length;
            for (int i = 0; i < CountTotal; i++) {
                if (ClassE[i] == ClassT[i]) {
                    acertos += 1;
                }
            }
            acc = (acertos * 100) / CountTotal;
        } else {
            this.acc = 0;
            this.acertos = 0;
        }
        return acc;
    }

    public void fitShow(int[] ClassE, int[] ClassT) {
        fit(ClassE, ClassT);
        System.out.println("Acertos: " + acertos + ", Precisão: " + acc);
    }

    public void fitShow(ArrayList<Avaliacao> avaliacoes) {
        if (avaliacoes != null && avaliacoes.size() > 0) {
            int[] ClassE = avaliacoes.get(0).getClasseTeste();
            int[][] ClassTM = new int[avaliacoes.get(0).getClassePredita().length][];
            for (int i = 0; i < ClassTM.length; i++) {
                ClassTM[i] = new int[avaliacoes.size()];
            }
            for (int i = 0; i < avaliacoes.size(); i++) {
                for (int j = 0; j < avaliacoes.get(i).getClassePredita().length; j++) {
                    ClassTM[j][i] = avaliacoes.get(i).getClassePredita()[j];
                }
            }
            int[] ClassT = new int[ClassTM.length];
            int Count = 0;
            for (int i = 0; i < ClassTM.length; i++) {
                ClassT[Count++] = ClassTM[i][(0 + ClassTM[i].length - 1) / 2];
            }
            fitShow(ClassE, ClassT);
        }
    }

    public double getAcc() {
        return acc;
    }

    public int getAcertos() {
        return acertos;
    }

    public static void main(String[] args) {
        TestRandom(10);
        Test(10);
    }

    /**
     *
     * @param classes
     * @test
     */
    public static void TestRandom(int classes) {
        if (classes == 0) {
            classes = 3;
        }
        Accuracy cm = new Accuracy(classes);
        Random r = new Random();
        int TF = r.nextInt(50);
        int[] ClassE = new int[TF];
        for (int i = 0; i < TF; i++) {
            ClassE[i] = r.nextInt(classes);
        }
        int[] ClassT = new int[TF];
        for (int i = 0; i < TF; i++) {
            ClassT[i] = r.nextInt(classes);
        }
        cm.fitShow(ClassE, ClassT);
    }

    /**
     *
     * @param classes
     * @test
     */
    public static void Test(int classes) {
        if (classes == 0) {
            classes = 3;
        }
        Accuracy cm = new Accuracy(classes);
        Random r = new Random();
        int TF = r.nextInt(50);
        int classe;
        int[] ClassE = new int[TF];
        int[] ClassT = new int[TF];
        for (int i = 0; i < TF; i++) {
            classe = r.nextInt(classes);;
            ClassE[i] = classe;
            ClassT[i] = classe;
        }
        cm.fitShow(ClassE, ClassT);
    }

}
