/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedeNeural.Camadas;

import RedeNeural.Neuronio.Neuronio;

/**
 *
 * @author Aluno
 */
public interface Camada {

    public void CalculaNet(double[] Camada);

    public double[] getNet();

    public void CalculaErroOcultas(double[] ErrosDaSaidas, double[][] PesosSaida);
    /*
    public void corrigePesosSaida(double N, Neuronio[] Oculta);

    public void corrigePesosOculta(double N, double[] CamadaEntrada);
   */
    public void corrigePesos(double txAp, double[] Camada);

    public void CalculoErroRede();

    public double[] CalculaNetSaidaTeste(double[] vOculta);

    public double getErro();

    public void CalculaErroSaida(int Desejado);
    
    public double[] getErroNeuronios();

    public double[][] getPesos();

    public int getLen();
    
    public Neuronio[] getNeuronios();
    
}
