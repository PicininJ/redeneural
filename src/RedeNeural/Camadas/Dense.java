/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedeNeural.Camadas;

import RedeNeural.Core.Variables;
import RedeNeural.Neuronio.Neuronio;
import java.util.Random;

/**
 *
 * @author Aluno
 */
public class Dense implements Camada {

    public Neuronio[] Neuronios;
    public double[][] Pesos;
    public int Tamanho;
    private double Erro;

    public Dense(int Oculta, int Entrada) {
        Neuronios = new Neuronio[Oculta];
        for (int i = 0; i < Oculta; i++) {
            Neuronios[i] = new Neuronio();
        }

        Pesos = new double[Oculta][Entrada];
        Tamanho = Oculta;
    }

   /* @Override
    public void corrigePesosOculta(double txAp, double[] aCamadaEntrada) {
        for (int i = 0; i < Neuronios.length; i++) {
            for (int j = 0; j < aCamadaEntrada.length; j++) {/////Modificado
                Pesos[i][j] = Pesos[i][j] + (txAp * Neuronios[i].getErro() * aCamadaEntrada[j]);
            }
        }
    }

    @Override
    public void corrigePesosSaida(double txAp, Neuronio[] Oculta) {
        for (int i = 0; i < Neuronios.length; i++) {
            for (int j = 0; j < Oculta.length; j++) {
                Pesos[i][j] = Pesos[i][j] + (txAp * Neuronios[i].getErro() * Oculta[j].getObtido());
            }
        }
    }*/

    @Override
    public void corrigePesos(double txAp, double[] Camada) {
        for (int i = 0; i < Neuronios.length; i++) {
            for (int j = 0; j < Camada.length; j++) {
                Pesos[i][j] = /*0.35**/(Pesos[i][j] + (txAp * Neuronios[i].getErro() * Camada[j]));
            }
        }
    }

    public void GeraPeso(int len) {
        Random num = new Random();
        double DesvioPadrao = 1 / Math.sqrt(Neuronios.length);
        for (int i = 0; i < Neuronios.length; i++) {
            for (int j = 0; j < len; j++) {
                if (Variables.typeStartPesos == 0) {
                    Pesos[i][j] = 1;
                } else if (Variables.typeStartPesos == 1) {
                    Pesos[i][j] = num.nextGaussian() * (DesvioPadrao + 0);
                } else {
                    Pesos[i][j] = num.nextInt(9) + 1;
                }
            }
        }
    }

    public void GeraPesoOld(int len) {
        Random num = new Random();
        for (int i = 0; i < Neuronios.length; i++) {
            for (int j = 0; j < len; j++) {
                Pesos[i][j] = (-2) + num.nextDouble() * (2 - (-2));
            }
        }
    }

    @Override
    public void CalculoErroRede() {
        double oErro = 0;
        for (int i = 0; i < Neuronios.length; i++) {
            oErro += Math.pow(Neuronios[i].getErro(), 2);
        }
        Erro = oErro * 0.5;
        /////Erro = oErro * Neuronios[0].getDerivada();/////derivada
    }

    @Override
    public void CalculaNet(double[] Camada) {
        for (int j = 0; j < Tamanho; j++) {
            Neuronios[j].calculaNet(Camada, Pesos, j);
            Neuronios[j].Calcula();
        }
    }

    @Override
    public double[] getNet() {
        /////Gerar Camada Oculta
        double[] vOculta = new double[Tamanho];
        for (int k = 0; k < Tamanho; k++) {
            vOculta[k] = Neuronios[k].getObtido();
        }
        return vOculta;
    }

    @Override
    public double[] getErroNeuronios() {
        /////Gerar Camada Oculta
        double[] vOculta = new double[Tamanho];
        for (int k = 0; k < Tamanho; k++) {
            vOculta[k] = Neuronios[k].getErro();
        }
        return vOculta;
    }

    @Override
    public void CalculaErroSaida(int Desejado) {
        /*int Pos = 0;

        double Val = Neuronios[Pos].getObtido();

        for (int k = 1; k < Tamanho; k++) {
            if (Neuronios[k].getObtido() > Val) {
                Val = Neuronios[k].getObtido();
                Pos = k;
            }
        }*/

        for (int k = 0; k < Tamanho; k++) {
            if (k == Desejado) {
                /*if (Pos == Desejado || Val == Neuronios[k].getObtido()) {
                    /////Neuronios[k].calculaErroSaida(Desejado, Desejado);
                    Neuronios[k].calculaErroSaida(Val, Val);
                } else {
                    /////Neuronios[k].calculaErroSaida(Desejado, Pos);
                    Neuronios[k].calculaErroSaida(Val, Neuronios[k].getObtido());
                }*/
                Neuronios[k].calculaErroSaidaT(Neuronios[k].getObtido());
            } else {
                ///////Neuronios[k].calculaErroSaida(Desejado, 0);
                ///////Neuronios[k].calculaErroSaida(0, Neuronios[k].getObtido());
                ///////Neuronios[k].calculaErroSaida(0, Pos);
                Neuronios[k].calculaErroSaidaNT(Neuronios[k].getObtido());
            }
        }
    }

    @Override
    public double[] CalculaNetSaidaTeste(double[] vOculta) {
        double[] vetObtido = new double[Tamanho];
        for (int j = 0; j < Tamanho; j++) {
            Neuronios[j].calculaNet(vOculta, Pesos, j);
            Neuronios[j].Calcula();
            vetObtido[j] = Neuronios[j].getObtido();
        }
        return vetObtido;
    }

    @Override
    public void CalculaErroOcultas(double[] ErrosDaSaidas, double[][] PesosSaida) {
        for (int j = 0; j < Tamanho; j++) {/////
            Neuronios[j].calculaErroOcultas(ErrosDaSaidas, PesosSaida, j);
        }
    }

    @Override
    public double getErro() {
        return Erro;
    }

    @Override
    public double[][] getPesos() {
        return Pesos;
    }

    @Override
    public int getLen() {
        return Neuronios.length;
    }

    @Override
    public Neuronio[] getNeuronios() {
        return Neuronios;
    }
}
