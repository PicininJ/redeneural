/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedeNeural.Interface;

import RedeNeural.Avaliacao.Accuracy;
import RedeNeural.Avaliacao.ConfusionMatrix;
import RedeNeural.Avaliacao.ErroRede;
import RedeNeural.Core.Dataset;
import RedeNeural.Core.RedeNeural;
import RedeNeural.Core.Variables;
import Utils.Animacao;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import Utils.util;
import com.sun.javafx.scene.control.skin.LabeledText;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.RandomAccessFile;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;

/**
 *
 * @author luis
 */
public class TelaPrincipalController implements Initializable {

    @FXML
    private TextField txEntrada;
    @FXML
    private TextField txSaida;
    @FXML
    private TextField txOculta;
    @FXML
    private TextField txValorErro;
    @FXML
    private TextField txNInteracoes;
    @FXML
    private TextField txN;
    @FXML
    private RadioButton rbLinear;
    @FXML
    private RadioButton rbLogistica;
    @FXML
    private RadioButton rbHiperbolica;
    @FXML
    private Button btCarregaArquivo;
    @FXML
    private ImageView imgStatus;
    @FXML
    private Label lblCaminho;
    @FXML
    private TableView<ObservableList> tabela;
    @FXML
    private RadioButton rbInteracao;
    @FXML
    private RadioButton rbErro;
    @FXML
    private AreaChart<?, ?> lcGrafico;
    @FXML
    private ImageView imgStatus1;
    @FXML
    private Button btCarregaTeste;
    @FXML
    private Button btnTreinar;
    @FXML
    private Button btnTestar;
    @FXML
    private Button btnAvanca;
    @FXML
    private TextArea txResultado;
    private static TextArea txbResultado;
    @FXML
    private TextField txKfold;
    @FXML
    private TableView<ObservableList> tabelaTeste;

    private Dataset Treino;
    private Dataset Teste;
    private String Funcaodetransferencia;
    private String Criteriodeparada;
    private RedeNeural rede;
    private RedeNeural redeNeural;
    private final ToggleGroup g1 = new ToggleGroup();
    private final ToggleGroup g2 = new ToggleGroup();
    private ArrayList<XYChart.Series<Number, Number>> seriesContainer = new ArrayList();
    @FXML
    private TableView<ObservableList> tabelaConfusao;
    @FXML
    private TextField txPrecisao;
    @FXML
    private TextField txAcertos;
    @FXML
    private Button btnTreinar1;
    private Dataset data;
    @FXML
    private TextField txTotal;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        iniciaComponentes();
        txbResultado = txResultado;
    }

    private void evtCarregaArquivo(MouseEvent event) {
        FileChooser fc = new FileChooser();
        fc.setTitle("Selecione o Arquivo de Treino");
        File f = fc.showOpenDialog(null);
        if (f != null) {
            lblCaminho.setText(f.getAbsolutePath());
            try {
                RandomAccessFile fr = new RandomAccessFile(f, "r");
                ArrayList<String> c = new ArrayList<String>();
                String line = fr.readLine();
                String[] atr = (line != null) ? line.split(",") : null;
                for (int i = 0; i < atr.length; i++) {
                    c.add(atr[i]);
                }
                util.criaColunasDinamicas(tabela, c);
                ///redeNeural = new RedeNeural(0, 0.001, 0.1, fr, 0, 0, 0);
                Platform.runLater(()
                        -> {
                    ///tabela.setItems(redeNeural.getCSV());
                });
            } catch (Exception ex) {
                System.out.println(ex.getCause());
            }
        }
    }

    @FXML
    private void evtAvanca(MouseEvent event) {
        exibeGraficoErro();
    }

    private void iniciaComponentes() {

        EventHandler<MouseEvent> eventHandler = (MouseEvent e)
                -> {
            if (e.getTarget() instanceof LabeledText) {
                String Text = ((LabeledText) e.getTarget()).getText();
                if (Text != null && !Text.trim().isEmpty()) {
                    if (Text.equalsIgnoreCase("Linear")
                            || Text.equalsIgnoreCase("Logística")
                            || Text.equalsIgnoreCase("Hiperbólica")) {
                        Funcaodetransferencia = Text;
                    } else {
                        Criteriodeparada = Text;
                    }
                }
            }
        };
        rbErro.setToggleGroup(g1);
        rbInteracao.setToggleGroup(g1);
        rbLinear.setToggleGroup(g2);
        rbHiperbolica.setToggleGroup(g2);
        rbLogistica.setToggleGroup(g2);

        rbErro.setOnMouseClicked(eventHandler);
        rbInteracao.setOnMouseClicked(eventHandler);

        rbHiperbolica.setOnMouseClicked(eventHandler);
        rbLinear.setOnMouseClicked(eventHandler);
        rbLogistica.setOnMouseClicked(eventHandler);

        MaskFieldUtil.numericField(txSaida);
        MaskFieldUtil.numericField(txEntrada);
        MaskFieldUtil.numericField(txOculta);
        MaskFieldUtil.numericField(txNInteracoes);

        //MaskFieldUtil.monetaryField(txN);
        //MaskFieldUtil.monetaryField(txValorErro);
        txEntrada.setText("6");
        txSaida.setText("3");
        txOculta.setText("3");
        txNInteracoes.setText("100");
        txValorErro.setText("0.0001");
        txN.setText("0.01");

        Funcaodetransferencia = rbLinear.getText();
        Criteriodeparada = rbInteracao.getText();
        //tx.setText("3");

        /*
        ArrayList<String> c = new ArrayList<String>();
        c.add("x1");
        c.add("x2");
        util.criaColunasDinamicas(tabela, c);
         */
        //tabela.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tabelaConfusao.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        btnTestar.setDisable(false);
        btnAvanca.setDisable(false);
        btnTreinar.setDisable(false);
        btCarregaTeste.setDisable(false);
    }

    private void exibeGraficoErro(ArrayList<ArrayList<Double>> erro) {
        Thread th = new Thread(()
                -> {
            Platform.runLater(()
                    -> {
                new Animacao(lcGrafico, erro).run();
            });
        });
        th.start();
    }

    private void exibeGraficoErro() {
        /*List<Double> l1 = new ArrayList<>();
        List<Double> l2 = new ArrayList<>();
        List<Double> l3 = new ArrayList<>();
        Random r = new Random();
        int max = 50;
        }
        for (int i = 0; i < max; i++)
        {
        l1.add(Math.cos(i) * 100);
        l2.add(Math.pow(i, 2));
        l3.add(Math.sin(i) * 100);
            });
        });
        th.start();
        }
        Thread th = new Thread(() ->
        {
        Platform.runLater(() ->
        {
        new Animacao(l1, l2, l3, lcGrafico).run();
        });
        });
        th.start();
         */
        ArrayList<Double> erros = rede.erroEpoca.get();
        Thread th = new Thread(() -> {
            Platform.runLater(() -> {
                new Animacao(erros/*, new ArrayList<>(), new ArrayList<>()*/, lcGrafico).run();
            });
        });
        th.start();
    }

    @FXML
    private void evtTreinar(MouseEvent event) {
        try {
            int Entrada = Integer.parseInt(txEntrada.getText());
            int Saida = Integer.parseInt(txSaida.getText());
            int Oculta = Integer.parseInt(txOculta.getText());
            double N = Double.parseDouble(txN.getText());
            int nInteracoes = Integer.parseInt(txNInteracoes.getText());
            double Erro = Double.parseDouble(txValorErro.getText());
            String Parada = Criteriodeparada;
            String FuncaodeTransferencia = Funcaodetransferencia;
            rede = new RedeNeural(Entrada, Saida, Oculta, N, nInteracoes, Parada,
                    FuncaodeTransferencia, Erro, Treino, Teste);
            rede.fit(Treino);
            /*btnTestar.setDisable(true);
            btnAvanca.setDisable(false);
            btnTreinar.setDisable(true);
            btCarregaTeste.setDisable(false);*/
            ////exibeGraficoErro(new ArrayList(rede.erroEpoca.get()));
        } catch (Exception ex) {
            Mensagem.Exibir("ERRO!", 2);
            ex.printStackTrace();
        }
    }

    private void exibeMatrizC() {
        ConfusionMatrix cm = rede.getMC();
        ArrayList<String> colunas = new ArrayList<>();
        for (Integer i = 0; i < cm.getMatrix()[0].length; i++) {
            colunas.add(i.toString());
        }
        util.criaColunasDinamicas(tabelaConfusao, colunas);
        tabelaConfusao.getItems().clear();
        ArrayList<String> linha;
        for (int i = 0; i < cm.getMatrix().length; i++) {
            linha = new ArrayList<>();
            for (int j = 0; j < cm.getMatrix()[i].length; j++) {
                linha.add(Double.toString(cm.getMatrix()[i][j]));
            }
            util.addLinha(tabelaConfusao, linha);
        }
        Accuracy acc = rede.getAcuracia();
        txTotal.setText(Integer.toString(tabelaTeste.getItems().size()));
        txPrecisao.setText(Double.toString(acc.getAcc()) + "%");
        txAcertos.setText(Integer.toString(acc.getAcertos()));
    }

    @FXML
    private void evtTestar(MouseEvent event) {
        try {
            rede.predict(Teste);
            exibeMatrizC();
        } catch (Exception ex) {
            Mensagem.Exibir("ERRO!", 2);
            ex.printStackTrace();
        }
    }

    @FXML
    private void evtCarregaArquivoTreino(MouseEvent event) {
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File("./Dataset"));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        File arq = fc.showOpenDialog(null);
        ArrayList a = new ArrayList<Object>();
        Treino = new Dataset();

        if (arq != null) {
            try {
                BufferedReader br = null;
                String linha = "";
                String csvDivisor = ",";
                int Count = 0;
                lblCaminho.setText(arq.getAbsolutePath());
                br = new BufferedReader(new FileReader(arq));
                tabela.getItems().clear();
                while ((linha = br.readLine()) != null) {
                    String[] res = linha.split(csvDivisor);

                    if (Count == 0) {
                        for (String re : res) {
                            Treino.addH(re);
                        }
                        util.criaColunasDinamicas(tabela, Arrays.asList(res));
                    } else {
                        for (String re : res) {
                            Treino.add(Double.parseDouble(re));
                        }
                        util.addLinha(tabela, Arrays.asList(res));
                        Treino.fit();
                    }
                    Count++;
                }
                /////Treino.show(txResultado);
                txEntrada.setText(Treino.getCamadaEntrada() + "");
                Integer Oculta = (Treino.getCamadaEntrada() + Integer.parseInt(txSaida.getText())) / 2;
                txOculta.setText(Oculta.toString());
                /*
                 btnTestar.setDisable(true);
                 btnAvanca.setDisable(true);
                btnTreinar.setDisable(false);*/
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    @FXML
    private void evtCarregaArquivoTeste(MouseEvent event) {
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File("./Dataset"));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        File arq = fc.showOpenDialog(null);
        Teste = new Dataset();
        tabelaTeste.getItems().clear();
        if (arq != null) {
            try {
                BufferedReader br = null;
                String linha = "";
                String csvDivisor = ",";
                int Count = 0;
                lblCaminho.setText(arq.getAbsolutePath());
                br = new BufferedReader(new FileReader(arq));
                while ((linha = br.readLine()) != null) {
                    String[] res = linha.split(csvDivisor);
                    if (Count == 0) {
                        for (String re : res) {
                            Teste.addH(re);
                        }
                        util.criaColunasDinamicas(tabelaTeste, Arrays.asList(res));
                    } else {
                        for (String re : res) {
                            Teste.add(Double.parseDouble(re));
                        }
                        util.addLinha(tabelaTeste, Arrays.asList(res));
                        Teste.fit();
                    }
                    Count++;
                }
                /*Teste.show(txResultado);
                btnTestar.setDisable(false);
                btnAvanca.setDisable(false);
                btnTreinar.setDisable(true);*/
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    public static void appendText(String Text) {
        Platform.runLater(() -> {
            txbResultado.appendText(Text);
        });
    }

    @FXML
    private void setPesosDefault(ActionEvent event) {
        Variables.typeStartPesos = 0;
    }

    @FXML
    private void setPesosGaussian(ActionEvent event) {
        Variables.typeStartPesos = 1;
    }

    @FXML
    private void setPesosSequencial(ActionEvent event) {
        Variables.typeStartPesos = 2;
    }

    @FXML
    private void setNormalizacaoSim(ActionEvent event) {
        Variables.typeNormalize = 0;
    }

    @FXML
    private void setNormalizacaoNao(ActionEvent event) {
        Variables.typeNormalize = 1;
    }

    @FXML
    private void evtkfold(MouseEvent event) {
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File("./Dataset"));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        File arq = fc.showOpenDialog(null);
        ArrayList a = new ArrayList<Object>();
        data = new Dataset();

        if (arq != null) {
            try {
                BufferedReader br = null;
                String linha = "";
                String csvDivisor = ",";
                int Count = 0;
                lblCaminho.setText(arq.getAbsolutePath());
                br = new BufferedReader(new FileReader(arq));
                while ((linha = br.readLine()) != null) {
                    String[] res = linha.split(csvDivisor);

                    if (Count == 0) {
                        for (String re : res) {
                            data.addH(re);
                        }
                        util.criaColunasDinamicas(tabela, Arrays.asList(res));
                    } else {
                        for (String re : res) {
                            data.add(Double.parseDouble(re));
                        }
                        util.addLinha(tabela, Arrays.asList(res));
                        data.fit();
                    }
                    Count++;
                }
                /////data.show(txResultado);

                txEntrada.setText(data.getCamadaEntrada() + "");
                Integer Oculta = (data.getCamadaEntrada() + Integer.parseInt(txSaida.getText())) / 2;
                txOculta.setText(Oculta.toString());
            } catch (Exception e) {
                System.out.println(e);
            }
        }

        try {
            int Entrada = Integer.parseInt(txEntrada.getText());
            int Saida = Integer.parseInt(txSaida.getText());
            int Oculta = Integer.parseInt(txOculta.getText());
            double N = Double.parseDouble(txN.getText());
            int nInteracoes = Integer.parseInt(txNInteracoes.getText());
            double Erro = Double.parseDouble(txValorErro.getText());
            int folds = Integer.parseInt(txKfold.getText());
            String Parada = Criteriodeparada;
            String FuncaodeTransferencia = Funcaodetransferencia;
            rede = new RedeNeural(Entrada, Saida, Oculta, N, nInteracoes, Parada,
                    FuncaodeTransferencia, Erro, Treino, Teste);
            /*ArrayList<ErroRede> erroRede = rede.fitKFolds(data, folds);
            ArrayList<ArrayList<Double>> erros = new ArrayList();
            for (ErroRede oerro : erroRede) {
                erros.add(oerro.get());
            }
            exibeGraficoErro(erros);*/
        } catch (Exception ex) {
            Mensagem.Exibir("ERRO!", 2);
            ex.printStackTrace();
        }
        exibeMatrizC();
    }

}
