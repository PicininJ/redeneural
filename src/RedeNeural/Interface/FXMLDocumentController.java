/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedeNeural.Interface;

import RedeNeural.Core.Dataset;
import RedeNeural.Core.RedeNeural;
import Utils.Mensagem;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 *
 * @author Raizen
 */
public class FXMLDocumentController implements Initializable {

    private Label label;
    @FXML
    private JFXTextField txbSaida;
    @FXML
    private JFXTextField txbEntrada;
    @FXML
    private JFXTextField txbOculta;
    @FXML
    private JFXTextField txbNumerodeInteracoes;
    @FXML
    private JFXTextField txbN;
    @FXML
    private JFXComboBox<String> cbCriteriodeparada;
    @FXML
    private JFXComboBox<String> cbFuncaodetransferencia;
    @FXML
    private JFXTextArea txbResultado;
    @FXML
    private JFXTextField txbErro;
    
    public static JFXTextArea txbRes;
    private Dataset Treino;
    private Dataset Teste;
    private RedeNeural rede;
    @FXML
    private JFXTextField txbClasses;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        cbCriteriodeparada.getItems().add("Por Iteração");
        cbCriteriodeparada.getItems().add("Por Erro");
        cbCriteriodeparada.getSelectionModel().selectFirst();
        cbFuncaodetransferencia.getItems().add("Linear");
        cbFuncaodetransferencia.getItems().add("Logística");
        cbFuncaodetransferencia.getItems().add("Hiperbólica");
        cbFuncaodetransferencia.getSelectionModel().selectFirst();
        txbRes = txbResultado;
    }

    @FXML
    private void evtCarregarTreino(MouseEvent event) {
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File("./Dataset"));
        fc.getExtensionFilters().add(new ExtensionFilter("CSV Files", "*.csv"));
        File arq = fc.showOpenDialog(null);
        Treino = new Dataset();
        if (arq != null) {
            try {
                BufferedReader br = null;
                String linha = "";
                String csvDivisor = ",";
                int Count = 0;
                br = new BufferedReader(new FileReader(arq));
                while ((linha = br.readLine()) != null) {
                    String[] res = linha.split(csvDivisor);
                    if (Count == 0) {
                        for (String re : res) {
                            Treino.addH(re);
                        }
                    } else {
                        for (String re : res) {
                            Treino.add(Double.parseDouble(re));
                        }
                        Treino.fit();
                    }
                    Count++;
                }
                /////Treino.show(txbResultado);
                txbEntrada.setText(Treino.getCamadaEntrada()+"");
                Integer Oculta = (Treino.getCamadaEntrada()+Integer.parseInt(txbSaida.getText()))/2;
                txbOculta.setText(Oculta.toString());
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    @FXML
    private void evtTreinar(MouseEvent event) {
        try {
            int Entrada = Integer.parseInt(txbEntrada.getText());
            int Saida = Integer.parseInt(txbSaida.getText());
            int Oculta = Integer.parseInt(txbOculta.getText());
            double N = Double.parseDouble(txbN.getText());
            int nInteracoes = Integer.parseInt(txbNumerodeInteracoes.getText());
            int Classes = Integer.parseInt(txbClasses.getText());
            double Erro = Double.parseDouble(txbErro.getText());
            String Parada = cbCriteriodeparada.getSelectionModel().getSelectedItem();
            String FuncaodeTransferencia = cbFuncaodetransferencia.getSelectionModel().getSelectedItem();
            rede = new RedeNeural(Entrada, Saida, Oculta, N, nInteracoes, Parada,
                    FuncaodeTransferencia, Erro, Treino, Teste);
            rede.fit(Treino);
        } catch (Exception ex) {
            Mensagem.Exibir("ERRO!", 2);
            ex.printStackTrace();
        }
    }

    @FXML
    private void evtCarregarTeste(MouseEvent event) {
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File("./Dataset"));
        fc.getExtensionFilters().add(new ExtensionFilter("CSV Files", "*.csv"));
        File arq = fc.showOpenDialog(null);
        Teste = new Dataset();
        if (arq != null) {
            try {
                BufferedReader br = null;
                String linha = "";
                String csvDivisor = ",";
                int Count = 0;
                br = new BufferedReader(new FileReader(arq));
                while ((linha = br.readLine()) != null) {
                    String[] res = linha.split(csvDivisor);
                    if (Count == 0) {
                        for (String re : res) {
                            Teste.addH(re);
                        }
                    } else {
                        for (String re : res) {
                            Teste.add(Double.parseDouble(re));
                        }
                        Teste.fit();
                    }
                    Count++;
                }
                /////Teste.show(txbResultado);
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    @FXML
    private void evtTestar(MouseEvent event) {
        try {
            rede.predict(Teste);
        } catch (Exception ex) {
            Mensagem.Exibir("ERRO!", 2);
            ex.printStackTrace();
        }
    }

}
