/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedeNeural.Core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javafx.scene.control.TextArea;

/**
 *
 * @author Raizen
 */
public class Dataset {

    private ArrayList<ArrayList<Double>> oDataset = new ArrayList();
    private ArrayList<Double> Valores;
    private ArrayList<String> Header;

    public void add(Double Valor) {
        if (Valores == null) {
            Valores = new ArrayList();
        }
        Valores.add(Valor);
    }

    public void addH(String header) {
        if (Header == null) {
            Header = new ArrayList();
        }
        Header.add(header);
    }

    public void fit() {
        if (Valores != null) {
            if (Variables.typeNormalize == 0) {
                List<Double> split = Valores.subList(0, Valores.size() - 1);///Remove a classe
                split = normalize(split);///Normaliza sem a Classe
                split.add(Valores.get(Valores.size() - 1));///Adiciona a Classe
                oDataset.add(new ArrayList(split));
            } else {
                oDataset.add(Valores);
            }
            Valores = null;
        }
    }

    public int getCamadaEntrada() {
        return Header.size() - 1;
    }

    public List<Double> normalize(List<Double> arr) {
        // find the max value
        double max = arr.get(0);

        double min = arr.get(0);

        //talvez ganhe mais desempenho pegando o menor e maior assim
        max = Collections.max(arr);
        min = Collections.min(arr);
        /*for (int x = 0; x < arr.size(); x++) {
            max = Math.max(max, arr.get(x));
        }
        for (int x = 0; x < arr.size(); x++) {
            min = Math.min(min, arr.get(x));
        }*/

        // normalize the array
        for (int x = 0; x < arr.size(); x++) {
            arr.set(x, (arr.get(x) - min) / (max - min));
        }

        return arr;
    }

    public void read() {
        System.out.println(Arrays.toString(Header.toArray()));
        for (ArrayList<Double> arr : oDataset) {
            System.out.println(Arrays.toString(arr.toArray()));
        }
    }

    public void show(TextArea txbResultado) {
        txbResultado.appendText(Arrays.toString(getH().toArray()) + "\n");
        for (ArrayList<Double> arr : oDataset) {
            for (Double db : arr) {
                txbResultado.appendText(String.format("%.2f", db) + "  ");
            }
            txbResultado.appendText("\n");
        }
    }

    public ArrayList<ArrayList<Double>> get() {
        return oDataset;
    }

    public ArrayList<String> getH() {
        return Header;
    }

    public double[][] getData() {
        double[][] data = new double[oDataset.size()][];
        for (int i = 0; i < oDataset.size(); i++) {
            data[i] = new double[oDataset.get(i).size()-1];
            for (int j = 0; j < oDataset.get(i).size()-1; j++) {
                data[i][j] = oDataset.get(i).get(j);
            }
        }
        return data;
    }

    public double[] getClasses() {
        double[] data = new double[oDataset.size()];
        for (int i = 0; i < oDataset.size(); i++) {
            data[i] = oDataset.get(i).get(oDataset.get(i).size()-1);
        }
        return data;
    }
}
