/*
 * MLP.java
 *
 * This class implements a multi layer perceptron network with
 * logistic activation functions and backpropagation algorithm.
 * 
 * @author Juliano Jinzenji Duque <julianojd@gmail.com>
 * @author Luiz Eduardo Virgilio da Silva <luizeduardovs@gmail.com>
 *
 * CSIM
 * Computing on Signals and Images on Medicine Group
 * University of Sao Paulo
 * Ribeirao Preto - SP - Brazil
 */
package RedeNeural.Core;

import RedeNeural.Avaliacao.Metrics;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class Network {

    protected int nInputs, nHidden, nOutput;  // Number of neurons in each layer
    protected double[/* i */] input, hidden, output;

    protected double[/* j */][/* i */] weightL1, // Weights values of connection between neuron "j"
            //     from hidden layer and "i" from input layer
            weigthL2;  // Weights values of connection between neuron "j"
    //     from output layer and "i" from hidden layer
    protected double learningRate = 0.02;

    /**
     * Gera uma instancia da MLP
     *
     * @param nInput number of neurons at input layer
     * @param nHidden number of neurons at hidden layer
     * @param nOutput number of neurons at output layer
     */
    public Network(int nInput, int nHidden, int nOutput) {

        this.nInputs = nInput;
        this.nHidden = nHidden;
        this.nOutput = nOutput;

        input = new double[nInput + 1];
        hidden = new double[nHidden + 1];
        output = new double[nOutput + 1];

        weightL1 = new double[nHidden + 1][nInput + 1];
        weigthL2 = new double[nOutput + 1][nHidden + 1];

        // Inicializa os Pesos
        generateRandomWeights();
    }

    /**
     * Set Taxa de Aprendizagem
     *
     * @param lr learning rate
     */
    public void setLearningRate(double lr) {
        learningRate = lr;
    }

    /**
     * Inicializa os pesos com valores aleatórios no intervalo de [-0.5,0.5]
     */
    protected void generateRandomWeights() {

        for (int j = 1; j <= nHidden; j++) {
            for (int i = 0; i <= nInputs; i++) {
                weightL1[j][i] = Math.random() - 0.5;
            }
        }

        for (int j = 1; j <= nOutput; j++) {
            for (int i = 0; i <= nHidden; i++) {
                weigthL2[j][i] = Math.random() - 0.5;
            }
        }
    }

    /**
     * Train the network with given a pattern. The pattern is passed through the
     * network and the weights are adjusted by backpropagation, considering the
     * desired output.
     *
     * @param pattern the pattern to be learned
     * @param desiredOutput the desired output for pattern
     * @return the network output before weights adjusting
     */
    public double[] train(double[] pattern, double[] desiredOutput) {
        double[] output = passNet(pattern);
        backpropagation(desiredOutput);

        return output;
    }

    /**
     * Passes a pattern through the network. Activatinon functions are
     * logistics.
     *
     * @param pattern pattern to be passed through the network
     * @return the network output for this pattern
     */
    public double[] passNet(double[] pattern) {

        for (int i = 0; i < nInputs; i++) {
            input[i + 1] = pattern[i];
        }

        // Set bias
        input[0] = 1.0;
        hidden[0] = 1.0;

        // net e gradiente Camada Oculta
        for (int j = 1; j <= nHidden; j++) {
            /////Soma
            hidden[j] = 0.0;
            for (int i = 0; i <= nInputs; i++) {
                hidden[j] += weightL1[j][i] * input[i];
            }
            /////Ativação
            hidden[j] = 1.0 / (1.0 + Math.exp(-hidden[j]));
        }

        // net e gradiente Camada de saida
        for (int j = 1; j <= nOutput; j++) {
            /////Soma
            output[j] = 0.0;
            for (int i = 0; i <= nHidden; i++) {
                output[j] += weigthL2[j][i] * hidden[i];
            }
            /////Ativação
            output[j] = 1.0 / (1 + 0 + Math.exp(-output[j]));
        }

        return Arrays.copyOfRange(output, 1, output.length);
    }

    /**
     * This method adjust weigths considering error backpropagation. The desired
     * output is compared with the last network output and weights are adjusted
     * using the choosen learn rate.
     *
     * @param desiredOutput desired output for the last given pattern
     */
    protected double backpropagation(double[] desiredOutput) {

        double[] errorL2 = new double[nOutput + 1];
        double[] errorL1 = new double[nHidden + 1];
        double errorAC = 0;
        double Esum = 0.0;

        for (int i = 1; i <= nOutput; i++) // Camada 2 error gradient
        {
            /////Função do erro
            errorL2[i] = output[i] * (1.0 - output[i]) * (desiredOutput[i - 1] - output[i]);
            errorAC += Math.pow(errorL2[i], 2);
        }
        errorAC *= 0.5;

        for (int i = 0; i <= nHidden; i++) {  // Camada 1 error gradient
            for (int j = 1; j <= nOutput; j++) {
                Esum += weigthL2[j][i] * errorL2[j];
            }

            /////Função do erro
            errorL1[i] = hidden[i] * (1.0 - hidden[i]) * Esum;
            Esum = 0.0;
        }

        /////Atualização dos pesos Oculta_Saida
        for (int j = 1; j <= nOutput; j++) {
            for (int i = 0; i <= nHidden; i++) {
                /////Função de Atualização
                weigthL2[j][i] += learningRate * errorL2[j] * hidden[i];
            }
        }

        /////Atualização dos pesos Entrada_Oculta
        for (int j = 1; j <= nHidden; j++) {
            for (int i = 0; i <= nInputs; i++) {
                /////Função de Atualização
                weightL1[j][i] += learningRate * errorL1[j] * input[i];
            }
        }

        return errorAC;
    }

    protected static ArrayList<Integer> getClasses(Dataset data) {
        ArrayList<Integer> asClasses = new ArrayList();
        for (int i = 0, Desejado; i < data.get().size(); i++) {
            ArrayList<Double> valor = data.get().get(i);
            Desejado = valor.get(valor.size() - 1).intValue();
            asClasses.add(Desejado);
        }
        return asClasses;
    }

    public static int generateClasses(Dataset... Data) {
        int numClasses = -1;
        if (Data != null) {
            ArrayList<Integer> asClasses = new ArrayList();
            for (Dataset dataset : Data) {
                if (dataset != null) {
                    asClasses.addAll(getClasses(dataset));
                }
            }
            numClasses = Metrics.count(asClasses);
        }
        return numClasses;
    }

    public static class MapClasses {

        protected static ArrayList<Integer> index;
        protected static ArrayList<Integer> aclasse;
        protected static Integer Count;

        public static int add(Integer classe) {
            init();
            int Classe;
            if (!index.contains(classe)) {
                index.add(classe);
                Classe = Count++;
                aclasse.add(Classe);
            } else {
                Classe = index.indexOf(classe);
            }
            return Classe;
        }

        public static void init() {
            if (index == null || aclasse == null) {
                if (index == null) {
                    index = new ArrayList();
                }
                if (aclasse == null) {
                    aclasse = new ArrayList();
                }
                Count = 0;
            }
        }

        public static Dataset map(Dataset data) {
            for (int i = 0, Desejado; i < data.get().size(); i++) {
                ArrayList<Double> valor = data.get().get(i);
                Desejado = valor.get(valor.size() - 1).intValue();
                data.get().get(i).set(valor.size() - 1, (double) add(Desejado));
            }
            return data;
        }

        public static ArrayList<Integer> getClasses() {
            return aclasse;
        }
    }

}
