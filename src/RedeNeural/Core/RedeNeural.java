package RedeNeural.Core;

import RedeNeural.Avaliacao.Accuracy;
import RedeNeural.Avaliacao.Avaliacao;
import RedeNeural.Avaliacao.ConfusionMatrix;
import RedeNeural.Avaliacao.ErroRede;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class RedeNeural extends Network{
    private int nInteracoes;
    private double Erro;
    private String Parada;
    private String FuncaodeTransferencia;
    private Dataset Treino;
    private Dataset Teste;
    private Avaliacao avaliacao;
    private int Classes;
    int TipoDeParada;
    private boolean verbose;
    public ErroRede erroEpoca;
    private ConfusionMatrix cm;
    private Accuracy acc;
    private Dataset Data;
    private double ErroRede;
    
    public RedeNeural(int nInput, int nHidden, int nOutput, double N, int nInteracoes, String Parada,
            String FuncaodeTransferencia, double Erro, Dataset Treino, Dataset Teste) {
        
        super(nInput, nHidden, nOutput);
        super.setLearningRate(N);
        
        this.nInteracoes = nInteracoes;
        this.Parada = Parada;
        this.FuncaodeTransferencia = FuncaodeTransferencia;
        this.Erro = Erro;
        verbose = true;

        //Neuronio.setFuncao(this.FuncaodeTransferencia);

        switch (this.Parada) {
            case "Interação":
                TipoDeParada = 1;
                break;
            case "Erro":
                TipoDeParada = 2;
                break;
            default:
                TipoDeParada = 1;
                break;
        }
    }
    
    public void fit(Dataset Treino) {
        this.Treino = MapClasses.map(Treino);
        Classes = generateClasses(Treino, Teste);

        double[] classes = Treino.getClasses();
        double[][] data = Treino.getData();
        
        fit(data, classes, Classes);
    }

    public ErroRede fit(double[][] data, double[] classes, int qtdClasses) {
        erroEpoca = new ErroRede();

        double ErroIteracao = 0;
        boolean First = true;
        int Count = 0;
        
        while (TipoDeParada == 1 && Count < nInteracoes
                || TipoDeParada == 2 && ErroRede >= Erro
                || Count < nInteracoes && First) {
            for (int i = 0; i < data.length /*&& ErroRede > Erro*/ || First; i++) {
                First = false;
                int Desejado = (int) classes[i];

                double[] classe = new double[qtdClasses];
                classe[(int) classes[i]] = 1.0f;
                double[] output = passNet(data[i]);
                ErroRede = backpropagation(classe);
                ErroIteracao += ErroRede;
            }
            if (verbose) {
                StringBuilder str = new StringBuilder().append("Itr: ").append(Count).append(" Erro Itr: ").append(ErroIteracao).append("\n");
                System.out.println(str);
            }

            erroEpoca.add(ErroIteracao);
            ErroIteracao = 0;
            Count++;
        }
        if (verbose) {
            System.out.println("Itr: " + nInteracoes);
        }
        return erroEpoca;
    }
    
    public void predict(Dataset Teste) {
        this.Teste = MapClasses.map(Teste);

        Classes = generateClasses(Treino, Teste);

        double[] classes = Treino.getClasses();
        double[][] data = Treino.getData();
        avaliacao = predict(data, classes, Classes, nOutput);

        cm = new ConfusionMatrix(Classes);
        cm.fitShow(avaliacao.getClasseTeste(), avaliacao.getClassePredita());
        acc = new Accuracy(Classes);
        acc.fitShow(avaliacao.getClasseTeste(), avaliacao.getClassePredita());
    }
    
    public Avaliacao predict(double[][] data, double[] classes, int qtdClasses, int Saida) {

        avaliacao = new Avaliacao();
        avaliacao.init(data.length);
        
        for (int i = 0; i < data.length; i++) {
            int Desejado = (int) classes[i];
            
            double[] classe = new double[qtdClasses];
            classe[(int) classes[i]] = 1.0f;
            double[] output = passNet(data[i]);
            avaliacao.predict(output, Saida, (int) classes[i]);

        }
        
        return avaliacao;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /////File arq = new File("D:\\master\\Dataset\\dataset\\Dataset_Treino_60.csv");
        File arq = new File("D:\\master\\Dataset\\treinamento.csv");
        Dataset Treino = new Dataset();
        if (arq != null) {
            try {
                BufferedReader br = null;
                String linha = "";
                String csvDivisor = ",";
                int Count = 0;
                br = new BufferedReader(new FileReader(arq));
                while ((linha = br.readLine()) != null) {
                    String[] res = linha.split(csvDivisor);
                    if (Count == 0) {
                        for (String re : res) {
                            Treino.addH(re);
                        }
                    } else {
                        for (String re : res) {
                            Treino.add(Double.parseDouble(re));
                        }
                        Treino.fit();
                    }
                    Count++;
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        /////arq = new File("D:\\master\\Dataset\\dataset\\Dataset_teste_40.csv");
        arq = new File("D:\\master\\Dataset\\teste.csv");
        Dataset Teste = new Dataset();
        if (arq != null) {
            try {
                BufferedReader br = null;
                String linha = "";
                String csvDivisor = ",";
                int Count = 0;
                br = new BufferedReader(new FileReader(arq));
                while ((linha = br.readLine()) != null) {
                    String[] res = linha.split(csvDivisor);
                    if (Count == 0) {
                        for (String re : res) {
                            Teste.addH(re);
                        }
                    } else {
                        for (String re : res) {
                            Teste.add(Double.parseDouble(re));
                        }
                        Teste.fit();
                    }
                    Count++;
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        /*Treino = RedeNeural.MapClasses.map(Treino);
        Teste = RedeNeural.MapClasses.map(Teste);
        int qtdClasses = RedeNeural.generateClasses(Treino, Teste);
        int Entrada = Treino.getCamadaEntrada();
        int Saida = qtdClasses;
        int Oculta = (Treino.getCamadaEntrada() + Saida) / 2;
        double N = 0.01f;
        int nInteracoes = 1000;
        MLP mlp = new MLP(Entrada, Oculta, Saida);

        double[] classes = Treino.getClasses();
        double[][] data = Treino.getData();
        mlp.fit(data, classes, qtdClasses);

        classes = Teste.getClasses();
        data = Teste.getData();
        Avaliacao avaliacao = mlp.predict(data, classes, qtdClasses, Saida);

        ConfusionMatrix cm = new ConfusionMatrix(qtdClasses);
        cm.fitShow(avaliacao.getClasseTeste(), avaliacao.getClassePredita());
        Accuracy acc = new Accuracy(qtdClasses);
        acc.fitShow(avaliacao.getClasseTeste(), avaliacao.getClassePredita());*/
    }
    
    public ConfusionMatrix getMC() {
        if (cm == null) {
            ConfusionMatrix cm = new ConfusionMatrix(Classes);
            cm.fitShow(avaliacao.getClasseTeste(), avaliacao.getClassePredita());
        }
        return cm;
    }

    public Accuracy getAcuracia() {
        if (acc == null) {
            Accuracy acc = new Accuracy(Classes);
            acc.fitShow(avaliacao.getClasseTeste(), avaliacao.getClassePredita());
        }
        return acc;
    }

}
