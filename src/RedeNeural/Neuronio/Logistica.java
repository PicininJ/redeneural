/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedeNeural.Neuronio;

import Utils.FuncaoTransferencia;

/**
 *
 * @author luis
 */
public class Logistica implements FuncaoTransferencia
{

    @Override
    public Double calculaF(Double net)
    {
        return 1 / (1 + Math.pow(Math.E, -net));
    }

    @Override
    public Double calculaFder(Double net)
    {
        return calculaF(net) * (1 - calculaF(net));
    }

}
