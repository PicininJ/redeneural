/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedeNeural.Neuronio;

/**
 *
 * @author Raizen
 */
public class Neuronio {

    private double net;
    private double erro;
    private double obtido = 0;
    protected double derivada;
    
    private static Funcao funcao = new Funcao() {};

    public static void setFuncao(String FuncaodeTransferencia) {

        switch (FuncaodeTransferencia) {
            case "Linear":
                funcao = new linear();
                break;
            case "Logística":
                funcao = new logistica();
                break;
            case "Hiperbólica":
                funcao = new hiperbolica();
                break;
            default:
                funcao = new Funcao() {
                };
                break;
        }
    }

    public void calculaNet(double[] camadas, double[][] pesos, int pos) {
        net = 0;
        for (int i = 0; i < camadas.length/* - 1*/; i++) {/////Modificado
            net += camadas[i] * pesos[pos][i];
        }
        getFuncao().setNet(net);
    }

    public void calculaErroSaida(int desejado, int ObtidoG) {
        erro = (desejado - ObtidoG) * getDerivada();
    }

    public void calculaErroSaida(double desejado, double ObtidoG) {
        erro = (desejado - ObtidoG) * getDerivada();
    }

    public void calculaErroSaidaT(double ObtidoG) {
        erro = (1.0f - ObtidoG) * (getDerivada() * ObtidoG);
    }

    public void calculaErroSaidaNT(double ObtidoG) {
        erro = (0.0f - ObtidoG) * (getDerivada() * ObtidoG);
    }

    public void calculaErroOcultas(double[] camadas, double[][] pesos, int pos) {
        erro = 0;
        for (int i = 0; i < camadas.length; i++) {
            erro += camadas[i] * pesos[i][pos];
        }

        erro = erro * funcao.getDerivada();
    }

    public void calculaErroOcultasOld(double[] camadas, double[][] pesos, int pos) {
        erro = 0;/////errado
        for (int i = 0; i < camadas.length/* - 1*/; i++) {
            erro += camadas[i] * pesos[i][pos];
        }

        erro = erro * funcao.getDerivada();
    }

    public static abstract class Funcao {

        protected double net;
        protected double obtido;
        protected double derivada;

        public void Calcula() {
            ///Default
            obtido = 1;
            derivada = 1;
        }

        public double getObtido() {
            return obtido;
        }

        public double getDerivada() {
            return derivada;
        }

        private void setNet(double net) {
            this.net = net;
        }
    }

    private static class linear extends Funcao {

        @Override
        public void Calcula() {
            obtido = net / 10.0;
            derivada = 0.1;
        }

    }

    private static class logistica extends Funcao {

        @Override
        public void Calcula() {
            obtido = (1 / (1 + Math.pow(Math.E, (-net))));
            derivada = obtido * (1 - obtido);
        }

    }

    private static class hiperbolica extends Funcao {

        @Override
        public void Calcula() {
            obtido = (((1 - (Math.pow(Math.E, (-2 * net))))) / (1 + (Math.pow(Math.E, (-2 * net)))));
            derivada = (1 - (Math.pow(obtido, 2)));
            /*
            obtido = (long)(obtido*1e6)/1e6;
            derivada = (long)(derivada*1e6)/1e6;
            */
        }

    }

    protected Funcao getFuncao() {
        return funcao;
    }

    /*public double getObtido() {
        return funcao != null ? funcao.getObtido() : 0;
    }*/
    
    public double getObtido() {
        return obtido;
    }
    
    public double Calcula() {
        getFuncao().Calcula();
        obtido = getFuncao().getObtido();
        derivada = getFuncao().getDerivada();
        return obtido;
    }
    
    public double getErro() {
        return erro;
    }
    
    public double getDerivada(){
        return derivada;
    }

}
