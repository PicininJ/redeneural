/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedeNeural.Neuronio;

import Utils.FuncaoTransferencia;

/**
 *
 * @author luis
 */
public class Linear implements FuncaoTransferencia
{

    @Override
    public Double calculaF(Double net)
    {
        return net / 10;
    }

    @Override
    public Double calculaFder(Double net)
    {
        return 0.1;
    }

}
