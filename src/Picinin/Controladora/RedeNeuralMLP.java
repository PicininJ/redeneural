/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Picinin.Controladora;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author luis
 */
public class RedeNeuralMLP
{
    private int epocas;
    private Double ValorDoErro;
    private Double taxaAprendizagem;
    
    
    
    private RandomAccessFile baseDados;
    private ObservableList<String[]> dados;

    //para calculos e controle
    
    /*private SimpleMatrix meo;//pesos da entrada para oculta
    private SimpleMatrix mos;//pesos da oculta para saida
    private SimpleMatrix ms;//matriz saida
    private SimpleMatrix mc;//matriz confusao*/

    public RedeNeuralMLP(int epocas, Double ValorDoErro, Double taxaAprendizagem, RandomAccessFile baseDados, int nentradas, int nocultos, int nsaidas)
    {
        this.epocas = epocas;
        this.ValorDoErro = ValorDoErro;
        this.taxaAprendizagem = taxaAprendizagem;
        this.baseDados = baseDados;
        
        /////
        
        /*meo = new SimpleMatrix(nentradas, nocultos);
        mos = new SimpleMatrix(nocultos, nsaidas);
        meo = new SimpleMatrix(nentradas, nocultos);*/
        
    }


    public void CarregaArquivoEmMemoria()
    {
        dados = FXCollections.observableArrayList();
        String line;
        try
        {
            baseDados.readLine();//pula primeira linha
            int i = 1;
            while ((line = baseDados.readLine()) != null)
                dados.add(line.split(","));

        } catch (IOException ex)
        {
            System.out.println(ex.getCause());
        }
    }
    public ObservableList<ObservableList> getCSV()
    {
        ObservableList<ObservableList> dados = FXCollections.observableArrayList();
        String line;
        String[] infos;

        try
        {
            baseDados.readLine();//pula primeira linha
            int i = 1;
            while ((line = baseDados.readLine()) != null)
            {
                infos = line.split(",");
                dados.addAll(FXCollections.observableArrayList(infos));
            }

        }
        catch (IOException ex)
        {
            Logger.getLogger(RedeNeuralMLP.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dados;
    }

    public boolean TreinaRede()
    {
        boolean flag = true;
        
        
        return flag;
    }
}
